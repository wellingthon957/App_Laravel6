@extends('admin.layout.app')
@section('title', 'Editanto Produtos')


@section('content')
<h2>Editando produtos {{$product->id}}</h2>  
<form action="{{ route("products.update", $product )}}" method="POST" enctype="multipart/form-data">
    @method('PUT')
    @include('admin.pages.products._partils.form') 
       {{-- <a href="{{ route('products.index') }}">Voltar</a> --}}
   </form>
@endsection