@extends('admin.layout.app')
@section('title', 'Detalhamento de Produtos')
@section('content')

    <h3>wo: {{ $product->wo }} do produto {{ $product->product }}</h3>
    <ul>
        <li><b>Wo em Produção:</b>  {{ $product->wo }}</li>
        <li><b>Descrição:</b> <td>{{ $product->description }}</td></li>
        <li><b>Quantidade:</b> <td>{{ $product->oty }}</td></li>
        <li><b>Status:</b> <td>{{ $product->status_wo }}</td></li>
        <li><b>Produto:</b> <td>{{ $product->product }}</td></li>
    </ul>
    <a href="{{ route('products.index') }}">Voltar</a>

    <form action="{{ route('products.destroy', $product->id)}}" method="POST">
        <input name="_method" type="hidden" value="DELETE">
        @csrf
        <div class="modal-footer">
            <button type="submit" class="btn btn-danger">Delete Produto {{ $product->product }}</button>
        </div>
    </form>
@endsection