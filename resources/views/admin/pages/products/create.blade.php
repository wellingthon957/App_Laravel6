@extends('admin.layout.app')
@section('title', 'Cadastrar Novos Produtos')

@section('content')
<a href="{{ route('products.index') }}">Voltar</a>
<h1>Cadastrar Novos Produtos</h1>
    <form action="{{ route("products.store")}}" method="POST" enctype="multipart/form-data" class="form">
         @include('admin.pages.products._partils.form')   
    </form>
@endsection