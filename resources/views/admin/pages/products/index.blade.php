@extends('admin.layout.app')
@section('title', 'Exibindo produtos')
    
{{-- @include('admin.includes.alert') --}}

{{-- @component('admin.components.card') --}}
@section('content')
<h1>Exibindo produtos</h1>
<a href="{{ route("products.create")}}" class="btn btn-dark">Cadastrar</a>
<br><br>


<form action="{{route('products.search')}}" method="POST" class="form">
    @csrf
    <input type="text" name="filter" placeholder="Buscar" class="form" value="{{ $filters['filter'] ?? null }}">
    <button type="submit" class="btn btn-primary">Buscar</button>
</form>


<table class="table table-striped">
    <thead class="thead-dark">
        <tr>
           <th>Wo:</th> 
           <th>Produto:</th>
            <th>Modelo:</th>
           <th>Status_Wo:</th>
           <th>Ações:</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($products as $product)
        <tr>
            <td>{{ $product->wo }}</td>
            <td>{{ $product->product }}</td>
            <td>{{ $product->model }}</td>
            <td>{{ $product->status_wo }}</td>
            <td>
                <a href="{{ route('products.show', $product->id) }}">Detalhar</a>
                <a href="{{ route('products.edit', $product->id) }}">Editar</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
    @if (isset($filters))
        {!! $products->appends($filters)->links() !!}
    @else
        {!! $products->links() !!}
    @endif

@endsection