<?php


Route::resource('products', 'ProductController')->middleware(['auth', 'check.is.admin']);
Route::any('products/search', 'ProductController@search')->name('products.search')->middleware('auth');

// Route::post('products', 'ProductController@store')->name('products.store');
// Route::get('products', 'ProductController@index')->name('products.index');
// Route::get('products/{id}', 'ProductController@show')->name('products.show');
// Route::get('products/create', 'ProductController@create')->name('products.create');
// Route::get('products/{id}/edit', 'ProductController@edit')->name('products.edit');
// Route::put('products/{id}', 'ProductController@update')->name('products.update');
// Route::delete('products/{id}', 'ProductController@destroy')->name('products.destroy');

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

