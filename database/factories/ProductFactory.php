<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'wo' => '123412',
        'product' => $faker->sentence(),
        'description' => $faker->sentence(),
        'model' => $faker->sentence(),
        'oty' => 123456,
        'status_wo' => $faker->sentence(),
    ];
});
