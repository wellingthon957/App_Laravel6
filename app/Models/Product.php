<?php

namespace App\models;

use Dotenv\Regex\Result;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    // protected $table = 'products';


    protected $fillable = ['wo','product', 'description', 'model', 'oty', 'status_wo', 'image'];


    public function search($filter = null)
    {
      
        $results = $this->where(function ($query) use($filter) {
            if($filter) {
                $query->where('wo', 'LIKE', "%{$filter}%");
            }
        })//->toSql();
        ->paginate();
        return $results;
    }
}
