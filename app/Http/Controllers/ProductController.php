<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUpdateProductRequest;
use App\models\Product;
use Illuminate\Http\Request;
use ProductsTableSeeder;

class ProductController extends Controller
{

    protected $request;
    protected $repository;


    public function __construct(Request $request, Product $product)
    {   
        $this->request = $request;
        $this->repository = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(9);

        return view('admin.pages.products.index', [
            'products' => $products,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUpdateProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateProductRequest $request)
    {
        $data = $request->only('wo','product', 'description', 'model', 'oty', 'status_wo', 'image');

        if($request->hasFile('image') && $request->image->isValid()) {
            dd('uplodad ok');
        };

        $this->repository->create($data);

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!$product = $this->repository->find($id))
        return redirect()->back();

        return view('admin.pages.products.show', [
            'product' => $product,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!$product = $this->repository->find($id))
        return redirect()->back();


        return view('admin.pages.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StoreUpdateProductRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$product = $this->repository->find($id))
        return redirect()->back();
        // dd("Editando o produto {$id}");
        $product->update($request->all());

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd("Deletando um produto $id");
        $product = $this->repository->find($id);
        $product->delete();
        return redirect()->route('products.index')->with(['success' => 'Invoice Deleted.']);
    }


    public function search(Request $request)
    {
        $filters = $request->all();
        $products = $this->repository->search($request->filter);

       return view('admin.pages.products.index', [
            'products'=> $products,
            'filters' => $filters,
       ]);

    }
}
