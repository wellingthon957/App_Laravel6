<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wo' => 'required|min:6|max:7|unique:products',
            'product' => 'required|min:2|max:10',
            'description' => 'nullable|min:3|max:1000',
            'model' => 'required|min:10|max:13',
            'oty' => "required|regex:/^\d+(\.\d{1.2})?$/",
            'status_wo' => 'required|min:6|max:11',
            'image' => 'nullable|image',

        ];
    }

    public function messages() 
    {
        return [
            'wo.required' => 'WO não pode ser Vazio!',
            'product.required' => 'PRODUTO não pode ser Vazio!',
            'model.required' => 'MODELO não pode ser Vazio!',
            'oty.required' => 'QUANTIDADE não pode ser Vazio',
            'status_wo.required' => 'STATUS nao pode ser vazio',
            'wo.unique' => 'ERRO! já existe essa wo'
            
        ];
    }
}
